﻿using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiscordBot
{
    public partial class Form1 : Form
    {
        private DiscordSocketClient _discordSocketClient = new DiscordSocketClient(new DiscordSocketConfig() { });
        private CommandService _commandService = new CommandService(new CommandServiceConfig() { });

        public Form1()
        {
            InitializeComponent();
        }

        private async void btnConnect_Click(object sender, EventArgs e)
        {
            await _commandService.AddModulesAsync(Assembly.GetEntryAssembly());

            _discordSocketClient.Connected += _discordSocketClient_Connected;
            _discordSocketClient.MessageReceived += _discordSocketClient_MessageReceived;
            await _discordSocketClient.LoginAsync(Discord.TokenType.Bot, "TOKEN HERE");
            await _discordSocketClient.StartAsync();
        }

        private async Task _discordSocketClient_Connected()
        {
            txtOutput.Text += $"Uh Oh, {Environment.NewLine} connected!";

            return;
        }

        private async Task _discordSocketClient_MessageReceived(SocketMessage arg)
        {
            var message = arg as SocketUserMessage;
            if (message == null)
                return;

            // Create a number to track where the prefix ends and the command begins
            int argPos = 0;
            // Determine if the message is a command, based on if it starts with '!' or a mention prefix
            if (!(message.HasCharPrefix('!', ref argPos) || message.HasMentionPrefix(_discordSocketClient.CurrentUser, ref argPos)))
                return;

            // Create a Command Context
            var context = new SocketCommandContext(_discordSocketClient, message);
            // Execute the command. (result does not indicate a return value, 
            // rather an object stating if the command executed successfully)
            var result = await _commandService.ExecuteAsync(context, argPos);

            if (!result.IsSuccess)
                await context.Channel.SendMessageAsync(result.ErrorReason);
        }

        private async void btnDisconnect_Click(object sender, EventArgs e)
        {
            await _discordSocketClient.LogoutAsync();
            await _discordSocketClient.StopAsync();
        }

        private async void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            await _discordSocketClient.LogoutAsync();
            await _discordSocketClient.StopAsync();
        }
    }
}
