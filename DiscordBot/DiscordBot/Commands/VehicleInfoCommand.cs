﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBot.Commands
{
    public class VehicleInfoCommand : ModuleBase<SocketCommandContext>
    {
        [Command("VehicleInfo")]
        [Summary("Returns information about a vehicle.")]
        public async Task VehicleInfoAsync([Remainder] [Summary("Registration")] string registration)
        {
            // ReplyAsync is a method on ModuleBase
            await ReplyAsync(registration + " would have been searched for if I was hooked up.");
            //this.Context.Message
            
        }
    }
}
