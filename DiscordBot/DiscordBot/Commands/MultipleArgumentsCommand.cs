﻿using Discord;
using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBot.Commands
{
    public class MultipleArgumentCommand : ModuleBase<SocketCommandContext>
    {
        [Command("multipleargument")]
        [Summary("Echoes a message with a string and number argument.")]
        public async Task EchoAsync([Summary("The text to echo")] string echo,[Remainder] [Summary("Number to echo")] int number)
        {
            await ReplyAsync($"{echo} - {number}");
        }
    }
}
